export interface Car {
  model: string;  
  plant: string;
  price: number;
  id?: number;
  }